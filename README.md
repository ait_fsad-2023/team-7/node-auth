# AIT Smoothies: Node Auth Example

### Submitted by: Sunil Prajapati
### ID: 124073

An example of node authentication using jsonwebtoken in express. 

### Requirments:

- Node
- Yarn
- Mongodb

### Running the app

First, install the packages using the command:

`yarn install`

Copy the .env.example as .env using the command:

`cp .env.example .env`

Update the `MONGODB_URI` in the .env file with your username and password:

`MONGODB_URI=mongodb+srv://<username>:<password>M@learning.hrhvy5z.mongodb.net/?retryWrites=true&w=majority`

To run the app, run the command:

`node app.js`

The app will run at https://localhost:3000
