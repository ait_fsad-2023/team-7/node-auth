const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');

dotenv.config();

const app = express();

app.use(cookieParser());
// middleware
app.use(express.static('public'));

// view engine
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// routes
app.get('/', (req, res) => res.render('home'));
app.get('/smoothies', (req, res) => res.render('smoothies'));

const authRoutes = require('./routes/authRoutes');
app.use(authRoutes);

// database connection
const dbURI = process.env.MONGODB_URI;
mongoose.connect(dbURI)
  .then((result) => {
    app.listen(3000)
    console.log("Server started at http://localhost:3000")
  })
  .catch((err) => console.log(err)); 
